package com.cihangir.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.cihangir.model.Book;


@Repository
public interface BookDAO {
    public void insertBook(Book book);

    public List<Book> getAllBooks();

    public Book findBook(int bookId);

    public void deleteBook(int bookId);

    public void updateBook(Book book);


}

