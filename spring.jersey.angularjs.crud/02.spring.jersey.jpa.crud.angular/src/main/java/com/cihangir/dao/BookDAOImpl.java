package com.cihangir.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.springframework.stereotype.Repository;

import com.cihangir.model.Book;


@Repository
public class BookDAOImpl implements BookDAO {

    @PersistenceContext
    public EntityManager entityManager;

    @Override
    public void insertBook(Book book) {
        entityManager.persist(book);
        entityManager.flush();
    }

    @Override
    public List<Book> getAllBooks() {
        String query="SELECT m FROM Book m";
        return this.entityManager.createQuery(query, Book.class).getResultList();

    }

    @Override
    public Book findBook(int bookId) {
        //return entityManager.find(Book.class, bookId);
        Book book = entityManager.find(Book.class, bookId);
        if (book != null) {
            return book;
        }else
            return null;
    }

    @Override
    public void deleteBook(int bookId) {
        Book book = entityManager.find(Book.class, bookId);
        if (book != null) {
            entityManager.remove(book);
            entityManager.flush();
        }
    }

    @Override
    public void updateBook(Book book) {
        entityManager.merge(book);
        entityManager.flush();
    }
}
