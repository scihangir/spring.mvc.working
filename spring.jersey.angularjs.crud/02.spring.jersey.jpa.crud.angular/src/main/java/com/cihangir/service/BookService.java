package com.cihangir.service;

import java.util.List;

import com.cihangir.model.Book;

/**
 * Created by cihangir on 3/3/17.
 */
public interface BookService {
    public void insertBook(Book book);
    public List<Book> getAllBooks();
    public Book findBook(int bookId);
    public void deleteBook(int bookId);
    public void updateBook(Book book);
}

