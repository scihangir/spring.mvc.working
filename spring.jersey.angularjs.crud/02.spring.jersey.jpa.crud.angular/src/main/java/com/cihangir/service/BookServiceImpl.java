package com.cihangir.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cihangir.dao.BookDAOImpl;
import com.cihangir.model.Book;


@Service
@Transactional
public class BookServiceImpl implements  BookService {
	
	@Autowired
	private BookDAOImpl bookDAOImpl;	
	
	@Override
	public void insertBook(Book book) {
		bookDAOImpl.insertBook(book);
	}

	@Override
	public List<Book> getAllBooks() {
		return bookDAOImpl.getAllBooks();
	}

	@Override
	public Book findBook(int bookId) {
		return bookDAOImpl.findBook(bookId);
	}

	@Override
	public void deleteBook(int bookId) {
		bookDAOImpl.deleteBook(bookId);
	}

	@Override
	public void updateBook(Book book) {
		bookDAOImpl.updateBook(book);
	}
}
