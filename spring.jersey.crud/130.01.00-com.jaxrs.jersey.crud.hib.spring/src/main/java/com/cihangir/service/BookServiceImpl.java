package com.cihangir.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cihangir.dao.BookDAOImpl;
import com.cihangir.model.Book;

@Service
public class BookServiceImpl implements BookService {

	@Autowired
	private BookDAOImpl bookDAOImpl;

	@Transactional
	public void insertBook(Book book) {
		bookDAOImpl.insertBook(book);
		System.out.println("Added succesfully!!!");
	}

	@Transactional
	public List<Book> getAllBooks() {
		return bookDAOImpl.getAllBooks();
	}

	@Transactional
	public Book findBook(int bookId) {
		return bookDAOImpl.findBook(bookId);
	}

	@Transactional
	public void deleteBook(int bookId) {
		bookDAOImpl.deleteBook(bookId);
	}

	@Transactional
	public void updateBook(Book book) {
		bookDAOImpl.updateBook(book);
	}

}
