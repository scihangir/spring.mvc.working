package com.cihangir.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cihangir.model.Book;

@Repository
public class BookDAOImpl implements BookDAO {

	@Autowired
	private SessionFactory sessionFactory;
	
	public BookDAOImpl() {
		System.out.println("Session:"+sessionFactory);
	}

	public Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public void insertBook(Book book) {
		Session session = getCurrentSession();
		session.persist(book);
	}

	public List<Book> getAllBooks() {
		Session session = sessionFactory.openSession();
		return session.createQuery("from Book").list();
	}

	public Book findBook(int bookId) {
		Session session = sessionFactory.openSession();
		Book book = (Book) session.get(Book.class, bookId);

		return book;

	}

	public void deleteBook(int bookId) {
		sessionFactory.getCurrentSession().createQuery("DELETE FROM Book WHERE id = " + bookId).executeUpdate();
	}

	public void updateBook(Book book) {
		if (book.getId() != 0) {
			sessionFactory.getCurrentSession().update(book);
		}
	}

}
