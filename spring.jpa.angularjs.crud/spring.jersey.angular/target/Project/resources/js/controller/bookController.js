/**
 * Created by cihangir on 2/25/17.
 */

angular.module('myApp')
    .controller('bookController', ['$scope', '$state', '$stateParams','bookService', function ($scope, $state, $stateParams,bookService) {

        bookService.getBooks().then(function (response) {
            $scope.books = response.data;
        });

        $scope.addBook = function(info){
            bookService.createBook(info).then(function (response) {
                $state.go("homepage");
            });
        };

        $scope.editBook = function(id,info){
            bookService.updateBook(id,info).then(function (response) {
                $state.go("homepage");
            });
        };

        $scope.deleteBook = function(info){
            bookService.removeBook(info).then(function (response) {
                
                $state.go($state.$current, null, { reload: true });
            });
        };


        $scope.fetchBook = function(){
            var id=$stateParams.id;
            bookService.showBook(id).then(function (response) {
                var book  = response.data;
                $scope.book = book;
            });
        };


    }]);