angular.module('myApp', ['ui.router'])
    .config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
        
        $stateProvider
        
            .state('homepage', {
                url: '',
                templateUrl: 'resources/js/templates/list.html'
            })

            .state('addBook', {
                url: '/add',
                templateUrl: 'resources/js/templates/add.html',
                controller: 'bookController'
            })


            .state('updateBook', {
                url: '/books/:id',
                templateUrl: 'resources/js/templates/edit.html',
                controller: 'bookController'
            });



        // .state('Homepage', {
            //     url: '/add',
            //     templateUrl: 'resources/js/templates/add.html'
            // })
        
    }]);