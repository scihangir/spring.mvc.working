/**
 * Created by cihangir on 2/25/17.
 */

angular.module('myApp')

    .factory('bookService', ['$http', function ($http) {
        
        return {
            getBooks: function () {
                return $http.get('/rest/book');
            },

            createBook: function (formData) {
                return $http.post('/rest/book', formData);
            },

            updateBook: function (id,formData) {
                return $http.put('/rest/book/'+id,formData);
            },

            showBook: function (id) {
                return $http.get('/rest/book/' +id);
            },

            removeBook: function (id) {
                return $http.delete('/rest/book/' +id);
            },

            

            // getCategories: function () {
            //     return $http.get('/category');
            // },

            // removeCategory: function (categoryId) {
            //     return $http.delete('/category/' + categoryId);
            // },
            // updateCategory: function (formData) {
            //     return $http.put('/category', formData);
            // }                                                                                                                                                                                                                                                                                                                                                                                                                    
        }
        
    }]);