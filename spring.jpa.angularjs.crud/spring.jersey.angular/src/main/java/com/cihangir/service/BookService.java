package com.cihangir.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cihangir.dao.BookDAO;
import com.cihangir.model.Book;


@Service
@Transactional
public class BookService {

	public  BookService() {

	}
	
	@Autowired
	private BookDAO bookDAO;	
	
	public void insertBook(Book book) {
		bookDAO.persist(book);
	}

	
	public List<Book> getAllBooks() {
		return bookDAO.findAll();
	}

	public Book findBook(int bookId) {
		return bookDAO.find(bookId);
	}


	public void deleteBook(int bookId) {
		bookDAO.remove(bookDAO.find(bookId));
	}

	public void updateBook(Book book) {
		bookDAO.merge(book);
	}
}
