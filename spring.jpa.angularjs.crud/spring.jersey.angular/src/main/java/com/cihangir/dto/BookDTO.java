package com.cihangir.dto;


public class BookDTO {

	
	private int id;
	private String bookTitle;
	private String category;
	private String author;
	private String ISBN;

	public BookDTO() {

	}

	public BookDTO(String bookTitle, String category, String author, String ISBN) {

		this.bookTitle = bookTitle;
		this.category = category;
		this.author = author;
		this.ISBN = ISBN;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getBookTitle() {
		return bookTitle;
	}

	public void setBookTitle(String bookTitle) {
		this.bookTitle = bookTitle;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getISBN() {
		return ISBN;
	}

	public void setISBN(String iSBN) {
		ISBN = iSBN;
	}

	@Override
	public String toString() {
		return "Book{" +
				"id=" + id +
				", bookTitle='" + bookTitle + '\'' +
				", category='" + category + '\'' +
				", author='" + author + '\'' +
				", ISBN='" + ISBN + '\'' +
				'}';
	}
}
