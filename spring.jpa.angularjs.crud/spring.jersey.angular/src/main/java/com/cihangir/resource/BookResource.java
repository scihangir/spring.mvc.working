package com.cihangir.resource;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cihangir.model.Book;
import com.cihangir.service.BookService;


@Component
@Path("/book")
public class BookResource {
	
	@Autowired
	private BookService bookService;
	
	public BookResource() {
		
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Book> getAll() {
		return bookService.getAllBooks();
	}

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Book get(@PathParam("id") int id) {
		return bookService.findBook(id);
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Book save(Book book) {
		bookService.insertBook(book);
		return book;
	}

	@PUT
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public void edit(@PathParam("id") int id, Book book) {
		book.setId(id);
		bookService.updateBook(book);
	}
	
	@DELETE  
    @Path("/{id}")  
    @Produces(MediaType.APPLICATION_JSON)  
	public void delete(@PathParam("id") int id) {
		bookService.deleteBook(id);
	}
	
	

}
