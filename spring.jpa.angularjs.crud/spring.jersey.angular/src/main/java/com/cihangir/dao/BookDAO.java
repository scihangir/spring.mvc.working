package com.cihangir.dao;


import org.springframework.stereotype.Repository;

import com.cihangir.model.Book;


@Repository
public class BookDAO extends GenericDAO<Book>{

	public BookDAO() {
		super(Book.class);
	}



}

