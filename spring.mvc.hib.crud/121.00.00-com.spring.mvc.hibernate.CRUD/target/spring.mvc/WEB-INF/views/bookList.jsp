<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title></title>
		<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	</head>
	
<body>
<div class="container">
	<div class="col-xs-11">
			<div class="article-area" >
				<div class="pull-right">
					<a href="addBook" class="btn btn-info btn-xs">
						Add Book
						<span class="glyphicon glyphicon-plus"></span>
					</a>				
				</div>

				<h2>Book List</h2>
				<div class="panel panel-default">
					<table class="table table-striped">
						<thead>
							<tr>
								<th>#</th>
								<th>Book Title</th>
								<th>Author</th>
								<th>Category</th>
								<th>ISBN</th>
								<th class="text-center">Action</th>
							</tr>
						</thead>

						<tbody>
							<!-- Kayitlari listeleyelim... -->
							<c:forEach items="${allBooks}" var="book">
								<tr>
									<td class="text-center">
										<c:out value="${book.id}" />
									</td>
									<td>
										<c:out value="${book.bookTitle}" />
									</td>
									<td>
										<c:out value="${book.author}" />
									</td>
									<td>
										<c:out value="${book.category}" />
									</td>
									<td>
										<c:out value="${book.ISBN }"/>
									</td>
									<td class="text-center">
		      							<a href="editBook?bookId=<c:out value='${book.id}'/>" class="btn btn-success">
		      								<span class="glyphicon glyphicon-edit"></span>
		      								Edit
										</a>
										<a href="deleteBook?bookId=<c:out value='${book.id}'/>" class="btn btn-danger">
											<span class="glyphicon glyphicon-trash"></span> 
											Delete
										</a>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
	</div>
</div>

	</body>
</html>