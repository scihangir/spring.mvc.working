package com.cihangir.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cihangir.model.Book;

@Repository
public class BookDAOImpl implements BookDAO {

	@Autowired
	private SessionFactory sessionFactory;

	public Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public void insertBook(Book book) {
		Session session = getCurrentSession();
		session.persist(book);
		System.out.println("Added succesfully!!!");
	}

	public List<Book> getAllBooks() {
		return sessionFactory.getCurrentSession().createQuery("FROM " + Book.class.getName()).list();
	}

	public Book findBook(int bookId) {
		return sessionFactory.getCurrentSession().get(Book.class, bookId);

	}

	public void deleteBook(int bookId) {
        sessionFactory.getCurrentSession().createQuery("DELETE FROM Book WHERE id = "+bookId).executeUpdate();    
	}

	public void updateBook(Book book) {
		System.out.println("Update book :::"+book.getId());
		if( book.getId() != 0 ) {
			sessionFactory.getCurrentSession().update(book); 
		} 
	}

}
