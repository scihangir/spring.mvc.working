package com.cihangir.controller;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.cihangir.model.Book;
import com.cihangir.service.BookService;

@Controller
public class BookController {

	@Autowired
	private BookService bookService;

    @RequestMapping(value = "/addBook", method = RequestMethod.GET)
    public ModelAndView setupForm() {

        System.out.println("Inside setupForm method...");
        ModelAndView modelAndView = new ModelAndView("addBook");
        modelAndView.addObject("newBook", new Book());

        return modelAndView;
    }

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String addBook(@ModelAttribute("newBook") Book book, Model model) {

		System.out.println("...addPerson...");

		bookService.insertBook(book);
		System.out.println("Added successfully!!!");

		return "redirect:/list";

	}

	@RequestMapping(value = {"/","/list"}, method = RequestMethod.GET)
	public String getPersonList(Model model) {

		System.out.println("...getBookList...");

		List<Book> bookList = bookService.getAllBooks();
		model.addAttribute("newBook", new Book());
		model.addAttribute("allBooks", bookList);

		return "bookList";
	}

	@RequestMapping(value = "/deleteBook{bookId}", method = RequestMethod.GET)
	public String deletePerson(@RequestParam("bookId") int id) {

		System.out.println("...deletePerson...");

		bookService.deleteBook(id);

		return "redirect:/list";
	}


	@RequestMapping(value = "/editBook{bookId}", method = RequestMethod.GET)
	public String editBook(@RequestParam("bookId") int id, Model model) {

		System.out.println("...editPerson ...");

		model.addAttribute("newBook", bookService.findBook(id));


		return "updateBook";
	}

	@RequestMapping(value = "/updateBook", method = RequestMethod.POST)
	public String updatePerson(@ModelAttribute("newBook") Book book) {

		System.out.println("...update person...");
		System.out.println("Heyy kardeş "+book.getId());

		bookService.updateBook(book);

		return "redirect:/list";
	}




}
