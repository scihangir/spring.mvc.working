<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
	<div class="col-xs-11">
			<div class="article-area" >
				<div class="pull-right">
					<a href="" class="btn btn-info btn-xs">
						<span class="glyphicon glyphicon-circle-arrow-left"></span>
						Go Back
					</a>
				</div>

				<h2>Add Book </h2>
				<div class="panel panel-default">
					<div class="panel-body">
						<form:form class="form-horizontal" method="post" modelAttribute="newBook" action="/add">
								<div class="form-group">
									<label for="bookTitle" class="col-sm-4 col-md-3 col-lg-3 control-label">Book Title</label>
									<div class="col-sm-8 col-md-6 col-lg-5">
										<input type="text" class="form-control" id="bookTitle" name="bookTitle">
									</div>
								</div>

								<div class="form-group">
									<label for="category" class="col-sm-4 col-md-3 col-lg-3 control-label">Category</label>
									<div class="col-sm-8 col-md-6 col-lg-5">
										<input type="text" class="form-control" id="category" name="category">
									</div>
								</div>

								<div class="form-group">
									<label for="author" class="col-sm-4 col-md-3 col-lg-3 control-label">Author</label>
									<div class="col-sm-8 col-md-6 col-lg-5">
										<input type="text" class="form-control" id="author" name="author">
									</div>
								</div>

								<div class="form-group">
									<label for="isbn" class="col-sm-4 col-md-3 col-lg-3 control-label">ISBN</label>
									<div class="col-sm-8 col-md-6 col-lg-5">
										<input type="text" class="form-control" id="ISBN" name="ISBN" value="yurt">
									</div>
								</div>

								<div class="form-group">
									<div class="col-sm-offset-4 col-md-offset-3 col-lg-offset-3 col-sm-8 col-md-9 col-lg-9">
										<button type="submit" class="btn btn-primary">Add</button>
									</div>
								</div>
						</form:form>
					</div>
				</div>
			</div>
	</div>
</div>
</body>
</html>
