<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>List Book</title>
		<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	</head>
	
<body>
<div class="container">
	<div class="col-xs-11">
			<div class="article-area" >
				<div class="pull-right">
					<a class="btn btn-info btn-xs" data-toggle="modal" data-target="#addBook" id="addBookModalBtn">
						Add Book
						<span class="glyphicon glyphicon-plus"></span>
					</a>				
				</div>

				<h2>Book List</h2>
				<div class="panel panel-default">
					<table class="table table-striped">
						<thead>
							<tr>
								<th>#</th>
								<th>Book Title</th>
								<th>Author</th>
								<th>Category</th>
								<th>ISBN</th>
								<th class="text-center">Action</th>
							</tr>
						</thead>

						   <tbody id="contentRows"></tbody>
					</table>
				</div>
			</div>
	</div>
</div>


<!-- add book modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="addBook">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">
					<span class="glyphicon glyphicon-plus-sign"></span>	Add Book
				</h4>
			</div><!--modal-header -->

			<form class="form-horizontal" id="createBookForm">
				<%@include file="addBook.jsp"%>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary" onclick="addBook();">Add</button>
				</div><!-- modal-footer -->
			</form>
		</div><!--modal-content -->
	</div><!--modal-dialog -->
</div><!--modal fade -->



<%--edit book modal--%>
<div class="modal fade" tabindex="-1" role="dialog" id="editBookModal">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">
					<span class="glyphicon glyphicon-plus-sign"></span>	Edit Book
				</h4>
			</div><!--modal-header -->

			<form class="form-horizontal">
				<div class="modal-body">
					<div class="edit-messages"></div>

					<div class="form-group">
						<label for="bookTitle" class="col-sm-4 col-md-3 col-lg-3 control-label">Book Title</label>
						<div class="col-sm-8 col-md-6 col-lg-5">
								<input type="text" class="form-control" id="editBookTitle"/>
						</div>
					</div>


					<div class="form-group">
						<label for="category" class="col-sm-4 col-md-3 col-lg-3 control-label">Category</label>
						<div class="col-sm-8 col-md-6 col-lg-5">
							<input type="text" class="form-control" id="editCategory"/>
						</div>
					</div>

					<div class="form-group">
						<label for="author" class="col-sm-4 col-md-3 col-lg-3 control-label">Author</label>
						<div class="col-sm-8 col-md-6 col-lg-5">
							<input type="text" class="form-control" id="editAuthor" />
						</div>
					</div>

					<div class="form-group">
						<label for="isbn" class="col-sm-4 col-md-3 col-lg-3 control-label">ISBN</label>
						<div class="col-sm-8 col-md-6 col-lg-5">
							<input type="text" class="form-control" id="editISBN"/>
						</div>
					</div>
				</div><!-- modal-body -->

				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary" onclick="editBook();">Save Changes</button>
					<input type="hidden" id="edit-bookId">
				</div><!-- modal-footer -->
			</form>
		</div><!--modal-content -->
	</div><!--modal-dialog -->
</div><!--modal fade -->


<!-- remove modal -->
	<div class="modal fade" tabindex="-1" role="dialog" id="removeBookModal">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div id="removeMessages"></div>
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title"><span class="glyphicon glyphicon-trash"></span> Remove Member</h4>
	      </div>
	      <div class="modal-body">
	        <p>Do you really want to remove ?</p>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        <button type="button" class="btn btn-primary" id="removeBtn">Save changes</button>
	      </div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
	<!-- /remove modal -->
	
	<script src="${pageContext.request.contextPath}/js/index.js"></script>
	</body>
</html>