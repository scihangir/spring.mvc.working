package com.cihangir.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;

import com.cihangir.model.Book;
import com.cihangir.service.BookService;

@Controller
public class BookController {

	@Autowired
	private BookService bookService;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String getPersonList() {
		return "bookList";
	}

	@RequestMapping(value = "/books", method = RequestMethod.GET)
	@ResponseBody
	public List<Book> getAllBook() {
		return bookService.getAllBooks();
	}

	@RequestMapping(value = "/deleteBook/{bookId}", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deletePerson(@PathVariable("bookId") int id) {
		bookService.deleteBook(id);
	}

	@RequestMapping(value = "/editBook/{bookId}", method = RequestMethod.GET)
	@ResponseBody
	public Book editBook(@PathVariable("bookId") int id) {
		Book book = bookService.findBook(id);
		return book;
	}

	@RequestMapping(value = "/book/{id}", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void putContact(@PathVariable("id") int id, @RequestBody Book book) {
		book.setId(id);
		bookService.updateBook(book);
	}

	@RequestMapping(value = "/book/{id}", method = RequestMethod.GET)
	@ResponseBody
	public Book getBook(@PathVariable("id") int id) {
		return bookService.findBook(id);
	}

	@RequestMapping(value = "/rest/add", method = RequestMethod.POST)
	@ResponseBody
	public Book addBookRest(@RequestBody Book book) {

		bookService.insertBook(book);
		return book;
	}

}
