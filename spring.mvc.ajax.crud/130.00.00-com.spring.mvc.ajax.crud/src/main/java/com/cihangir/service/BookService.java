package com.cihangir.service;

import java.util.List;

import com.cihangir.model.Book;

public interface BookService {
	public void insertBook(Book book);
	public List<Book> getAllBooks();
    public Book findBook(int bookId);
    public void deleteBook(int bookId);
    public void updateBook(Book book);


}
