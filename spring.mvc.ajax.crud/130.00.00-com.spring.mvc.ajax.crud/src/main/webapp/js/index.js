$(document).ready(function () {
    	loadBooks();
});

	function editBook() {
		 $.ajax({
	            type: 'PUT',
	            url: 'book/' + $('#edit-bookId').val(),
	            data: JSON.stringify({
	            	bookTitle: $('#editBookTitle').val(),
					category: $('#editCategory').val(),
					author: $('#editAuthor').val(),
					isbn: $('#editISBN').val()
	            }),
	            headers: {
	                'Accept': 'application/json',
	                'Content-Type': 'application/json'
	            },
	            'dataType': 'json'
	        }).success(function () {
	            loadBooks();
	            $(".edit-messages").html('<div class="alert alert-success alert-dismissible" role="alert">'+
						  '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
						  '<strong> <span class="glyphicon glyphicon-ok-sign"></span> </strong>'+"Successfully Updated"+
						'</div>');
	            $('editBookModal').modal('hide');
	        });
	}
	
	function addBook() {
		$.ajax({
			type: 'POST',
			url: '/rest/add',
			data: JSON.stringify({
				bookTitle: $('#add-bookTitle').val(),
				category: $('#add-category').val(),
				author: $('#add-author').val(),
				isbn: $('#add-ISBN').val()
			}),
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			success:function() {
					$("#messages").html('<div class="alert alert-success alert-dismissible" role="alert">'+
							'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
							'<strong> <span class="glyphicon glyphicon-ok-sign"></span> </strong>'+"Successfully Added"+
							'</div>');
					loadBooks();

			}	


		});
	}/*ends function:addBook()*/
	
	function deleteBook(id) {		
		// click on remove button
		$("#removeBtn").unbind('click').bind('click', function() {
			$.ajax({
				url: 'deleteBook/'+id,
				type: 'delete',
				dataType: 'json',
				success:function() {		
						$("#removeMessages").html('<div class="alert alert-success alert-dismissible" role="alert">'+
							  '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
							  '<strong> <span class="glyphicon glyphicon-ok-sign"></span> </strong>'+"Success"+
							'</div>');

						// close the modal
						$("#removeBookModal").modal('hide');
						
						loadBooks();
				}
			});

		});
	}/*ends function:deleteBook*/
	
	function loadBooks() {
	        $.ajax({
	           url:"books"
	        }).success(function (data, status) {
	            fillBookTable(data, status);
	        });
	}/*ends function:loadBooks*/
	
	function fillBookTable(bookList, status) {
	    // clear the previous list
	    clearContactTable();
	    // grab the tbody element that will hold the new list of books
	    var cTable = $('#contentRows');

	    var count=1;

	    // render the new contact data to the table
	    $.each(bookList, function (index, book) {
	        cTable.append($('<tr>')
	                .append($('<td>').text(count))
	                .append($('<td>').text(book.bookTitle))
	                .append($('<td>').text(book.category))
	                .append($('<td>').text(book.author))
	                .append($('<td>').text(book.isbn))
	                .append($('<td class="text-center">')
	                        .append($('<a>')
	                                .attr({
	                                    'type':'button',
	                                    'class':'btn btn-success',
	                                    'data-book-id': book.id,
	                                    'data-toggle': 'modal',
	                                    'data-target': '#editBookModal'
	                                })
	                                .text('Edit ')
	                                .append($('<span>')
	                                        .attr({
	                                            'class':'glyphicon glyphicon-edit'
	                                        })     
	                                )//ends the <span> tag
	                                   
	                         ) // ends the <a> tag
	                    
	                        .append($('<a>')
	                                .attr({
	                                    'type':'button',
	                                    'class':'btn btn-danger',
	                                    'data-toggle': 'modal',
	                                    'data-target': '#removeBookModal',
	                                    'onclick':'deleteBook('+book.id+')'
	                                })
	                                .text('Delete ')
	                                .append($('<span>')
	                                        .attr({
	                                            'class':'glyphicon glyphicon-trash'
	                                        })     
	                                )//ends the <span> tag
	                                   
	                         ) // ends the <a> tag
	                ) // ends the <td> tag for Delete

	                    
	                ); // ends the <tr> for this Contact
	                count++;
	    }); // ends the 'each' function
	}/*ends function:fillBookTable*/
	
	// Clear all content rows from the summary table
	function clearContactTable() {
	    $('#contentRows').empty();
	}/*ends function:clearContactTable*/
		
	$('#editBookModal').on('show.bs.modal', function (event) {
	    var element = $(event.relatedTarget);
	    var bookId = element.data('book-id');
	    var modal = $(this);
	    $.ajax({
	        type: 'GET',
	        url: 'book/' + bookId
	    }).success(function (book) {
	    	modal.find('#edit-bookId').val(book.id);
	        modal.find('#editBookTitle').val(book.bookTitle);
	        modal.find('#editCategory').val(book.category);
	        modal.find('#editAuthor').val(book.author);
	        modal.find('#editISBN').val(book.isbn);
	    });
	});
	
	
		