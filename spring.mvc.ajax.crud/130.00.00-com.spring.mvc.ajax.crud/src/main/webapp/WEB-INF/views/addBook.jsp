<div class="modal-body">
    <div id="messages"></div>

    <div class="form-group">
        <label for="bookTitle" class="col-sm-4 col-md-3 col-lg-3 control-label">Book Title</label>
        <div class="col-sm-8 col-md-6 col-lg-5">
            <input type="text" class="form-control" id="add-bookTitle" name="bookTitle">
        </div>
    </div>


    <div class="form-group">
        <label for="category" class="col-sm-4 col-md-3 col-lg-3 control-label">Category</label>
        <div class="col-sm-8 col-md-6 col-lg-5">
            <input type="text" class="form-control" id="add-category" name="category">
        </div>
    </div>

    <div class="form-group">
        <label for="author" class="col-sm-4 col-md-3 col-lg-3 control-label">Author</label>
        <div class="col-sm-8 col-md-6 col-lg-5">
            <input type="text" class="form-control" id="add-author" name="author">
        </div>
    </div>

    <div class="form-group">
        <label for="isbn" class="col-sm-4 col-md-3 col-lg-3 control-label">ISBN</label>
        <div class="col-sm-8 col-md-6 col-lg-5">
            <input type="text" class="form-control" id="add-ISBN" name="ISBN" value="yurt">
        </div>
    </div>
</div><!-- modal-body -->